import os

def get_project_tree(directory, indent=''):
    tree = ''
    for root, dirs, files in os.walk(directory):
        level = root.replace(directory, '').count(os.sep)
        current_indent = indent + '|     ' * level + '|-- '
        tree += f"{current_indent}{os.path.basename(root)}\n"
        sub_indent = indent + '|     ' * (level + 1)
        for file in files:
            tree += f"{sub_indent}|-- {file}\n"
    return tree

working_directory = os.getcwd()
project_tree = get_project_tree(working_directory, indent='.')

output_file = 'project_tree.txt'

with open(output_file, 'w') as file:
    file.write(project_tree)